import MerkleTree from 'merkletreejs'
import * as openpgp from 'openpgp'
import SHA256 from 'crypto-js/sha256'

const constructProof = function( path, neighbors){
  let proof = []

  neighbors.forEach((neighbor, i) => {
    if(neighbor === null){
      var data = Buffer.from(new Uint8Array(256/8))
    } else {
      var data = Buffer.from(openpgp.util.hex_to_Uint8Array(neighbor))
    }
    let position = path[i] === '1' ? 'left' : 'right'
    proof.push({data, position})
  })
  return proof
}

export const verifyProof = async function(email, public_key, merkleRoot, neighbors){
  let hash = await openpgp.crypto.hash.sha256(email)
  let path = Array.from(hash).map(x => ('00000000' + x.toString(2)).substr(-8)).join('')
  const leaves = [].map(x => SHA256(x))
  let tree = new MerkleTree(leaves, SHA256)
  public_key = openpgp.util.hex_to_Uint8Array(public_key)
  public_key = await openpgp.crypto.hash.sha256(public_key)
  public_key = Buffer.from(public_key)
  merkleRoot = openpgp.util.hex_to_Uint8Array(merkleRoot)
  neighbors= neighbors.reverse()
  path = Array.from(path).reverse().join('')
  let isValidProof = tree.verify(constructProof(path, neighbors), public_key, merkleRoot)
  return isValidProof

}
